const { myQApi } = require('@hjdhjd/myq');
const express = require('express');
const app = express();

function getFromConfigJson(key) {
    const config = require('./config.json');

    return config[key];
}

var myq = new myQApi(
    process.env.USERNAME ?? getFromConfigJson('username'), 
    process.env.PASSWORD ?? getFromConfigJson('password'));

app.get('/', (_, res) => {
    res.json({ message: 'Hello world' });
});

app.get('/garage-status', async (req, res) => {
    if (!isAccessKeyValid(req.query.accessKey)) {
        return res.json({ message: 'Unauthorized' }, 401);
    }

    res.json((await getGarageDoor()).state.door_state);
});

function isAccessKeyValid(providedKey) {
    const expected = process.env.ACCESS_KEY ?? getFromConfigJson('accessKey');

    return expected === providedKey;
}

async function getGarageDoor() {
    const serialNumber = process.env.GARAGE_DOOR_SERIAL ?? getFromConfigJson('garageDoorSerial');

    await myq.refreshDevices();
    return myq.devices.find(x => x['serial_number'] === serialNumber);
}

app.get('/toggle', async (req, res) => {
    if (!isAccessKeyValid(req.query.accessKey)) {
        return res.json({ message: 'Unauthorized' }, 401);
    }

    const garage = await getGarageDoor();

    let newState = 'ERR';
    if (garage.state.door_state === 'open') {
        newState = 'close';
    } else if (garage.state.door_state === 'closed') {
        newState = 'open';
    }

    if (newState === 'ERR') {
        res.json({ message: "Looks like the door is either opening or closing. myQ won't let us open/close the door until it has finished the current operation." }, 400);
        return;
    }

    await myq.execute(garage, newState);
    res.json({ message: 'Complete' }, 200);
})

app.get('/open', async (req, res) => {
    if (!isAccessKeyValid(req.query.accessKey)) {
        return res.json({ message: 'Unauthorized' }, 401);
    }

    var garage = await getGarageDoor();

    switch (garage.state.door_state) {
        case 'open':
            res.json({ message: 'Door already open.' }, 400);
            break;
        case 'closed':
            await myq.execute(garage, 'open');
            res.json({ message: 'Complete' }, 200);
            break;
        default:
            res.json({ message: "Looks like the door is either opening or closing. myQ won't let us open/close the door until it has finished the current operation." }, 400);
            break;
    }
});

app.get('/close', async (req, res) => {
    if (!isAccessKeyValid(req.query.accessKey)) {
        return res.json({ message: 'Unauthorized' }, 401);
    }

    var garage = await getGarageDoor();

    switch (garage.state.door_state) {
        case 'open':
            await myq.execute(garage, 'close');
            res.json({ message: 'Complete' }, 200);
            break;
        case 'closed':
            res.json({ message: 'Door already closed.' }, 400);
            break;
        default:
            res.json({ message: "Looks like the door is either opening or closing. myQ won't let us open/close the door until it has finished the current operation." }, 400);
            break;
    }
});

app.listen(3000, () => {
    myq.refreshDevices();
    console.log(`Example app listening on port 3000`);
});